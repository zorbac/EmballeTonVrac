# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import jinja2
import logging
import os
import shutil
import json
from json2html import json2html


def generate_cmd_file(data):
    if True:    
        logger = logging.getLogger(__name__)

        commande_info_formate_s=data["INFO"]
        dict_nettoyage=data["CMD"] 

        dict_contenants_utilise=data['CONTENANTS']

        dict_cmde =data['COMMAND_DETAIL'] 

        json_commande = json.dumps(dict_nettoyage, indent=2, sort_keys=True)

        if os.path.exists("/mnt/tmpfs"):
            dossier_sauvegarde_sortie = "/mnt/tmpfs"
        else:
            dossier_sauvegarde_sortie = os.path.realpath(
                os.path.dirname(__file__))

        fichier_sortie = os.path.join(
            dossier_sauvegarde_sortie, "commande_prepare_avant.html")

        output_cmde = json2html.convert(
            json=json_commande, table_attributes="id=\"info-table\" border=\"1\" class=\"table table-condensed table-bordered table-hover\"", clubbing=True, encode=True, escape=False)

        output_cmd = json2html.convert(
            json.dumps(
                dict_cmde, indent=2, sort_keys=True), table_attributes="id=\"info-table\" border=\"1\" class=\"table table-condensed table-bordered table-hover\"", clubbing=True, encode=True, escape=False)
                
        output_contenants = json2html.convert(
            json=json.dumps(
                dict_contenants_utilise, indent=2, sort_keys=True), table_attributes="id=\"info-table\" border=\"1\" class=\"table table-condensed table-bordered table-hover\"", clubbing=True, encode=True, escape=False)
                

        with open(fichier_sortie, "w") as file:
            file.write(str(commande_info_formate_s))
            file.write(str(output_cmde)[2:-1])
            file.write("<BR>\nListe contenant:\n<BR>\n")
            file.write(str(output_contenants)[2:-1])
            file.write("<BR>\nCommande:\n<BR>\n")
            file.write(str(output_cmd)[2:-1])

#        os.system("xdg-open {} &".format(fichier_sortie))

