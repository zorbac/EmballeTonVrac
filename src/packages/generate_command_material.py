# -*- coding: latin-1 -*-
'''
@author: jingl3s

'''
# license
#
# Copyright jingl3s 
# This code is free software; you can redistribute it and/or modify it
# under the terms of the BSD license (see the file
# COPYING.txt included with the distribution).

import datetime
import jinja2
import logging
import os
import shutil



class GenerateCommandMaterial:
    '''
    Construction affichage pour les bus
    '''
    BASE_NOM_FICHIER = "commande_prepare"
    FICHIER_SORTIE = BASE_NOM_FICHIER + ".html"
    DOSSIER_TPL = "resources"
    TPL_FILE = "commande.html.jinja2"
    DOSSIER_CIBLE = "sorties"

    def __init__(self):
        '''
        RAS
        '''
        # Template access
        self._tpl_path = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                self.DOSSIER_TPL))
        if not os.path.exists(self._tpl_path):
            self._tpl_path = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                '..',
                self.DOSSIER_TPL))
        if not os.path.exists(self._tpl_path):
            raise ValueError("Template file not found {}",format(self.DOSSIER_TPL))
        self._heure_courante = 0
        self._affiche_navigateur = False
        self._logger = logging.getLogger(self.__class__.__name__)

        if os.path.exists("/mnt/tmpfs"):
            self._dossier_cible = "/mnt/tmpfs"
        else:
            dossier_sauvegarde_sortie = os.path.realpath(
                os.path.dirname(__file__))

            self._dossier_cible = os.path.abspath(
            os.path.join(
                os.path.dirname(__file__),
                "..",
                self.DOSSIER_CIBLE))
        self._dernier_extinction_ecran = None
        self._nom_dossier_ressources_utiliser = ""

    def set_dossier_cible(self, p_chemin_sortie):
        self._dossier_cible = p_chemin_sortie
        self._logger.debug(
            "New output files directory {}".format(self._dossier_cible))

    def set_nom_dossier_ressources_utiliser(self, p_dossier: str):
        self._nom_dossier_ressources_utiliser = p_dossier

    def set_config(self, p_config):
        pass
        
    def set_open_browser(self, enable:bool):
        self._affiche_navigateur = enable
        
    def affiche_obtenir_contenu(self, data):
        output_text = self._set_jinja_info(data)

        return output_text

    def generate_file(self, data):

        output_text = self.affiche_obtenir_contenu(data)

        if not os.path.exists(self._dossier_cible):
            os.mkdir(self._dossier_cible)

        with open(os.path.join(self._dossier_cible, self.FICHIER_SORTIE), 'w') as fichier:
            fichier.write(output_text)
        if self._affiche_navigateur:

            os.system(
                "xdg-open " + os.path.join(self._dossier_cible, self.FICHIER_SORTIE) + " &")
            self._affiche_navigateur = False

    def _set_jinja_info(self, data):
        '''
        source : http://kagerato.net/articles/software/libraries/jinja-quickstart.html
        :param dict_horaires_bus:
        '''

        # In this case, we will load templates off the filesystem.
        # This means we must construct a FileSystemLoader object.
        #
        # The search path can be used to make finding templates by
        #   relative paths much easier.  In this case, we are using
        #   absolute paths and thus set it to the filesystem root.
        templateLoader = jinja2.FileSystemLoader(
            searchpath=self._tpl_path)

        # An environment provides the data necessary to read and
        #   parse our templates.  We pass in the loader object here.
        # Disable any whitespace due to jinja syntax
        templateEnv = jinja2.Environment(loader=templateLoader, trim_blocks=True, lstrip_blocks=True)

        # This constant string specifies the template file we will use.
        TEMPLATE_FILE = self.TPL_FILE

        # Read the template file using the environment object.
        # This also constructs our Template object.
        template = templateEnv.get_template(TEMPLATE_FILE)

        dt_current_date_time = datetime.datetime.now()

        # Specify any input variables to the template as a dictionary.
        templateVars = {
            "date_creation_fichier": dt_current_date_time.strftime("%d/%m/%Y %H:%M")}
            
        self._logger.debug(data)
        templateVars["data"] = data

        # Finally, process the template to produce our final text.
        output_text = template.render(templateVars)

        return output_text
