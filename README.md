# Emballe ton vrac
Permet de définir de manière simple des contenant en fonction d'une commandes

Etat: Embryonnaire

Application: Partiel sur le site nousrire.com

## Pour commencer

### Requis
    Python 3.x

### Installation
```shell
git clone https://gitlab.com/jingl3s/EmballeTonVrac.git
cd EmballeTonVrac
pip3 install -f requirements.txt
```

### Configuration

#### Fichier de configuration

configuration/nousrire_emballages_config.csv

Explications

- A faire


## Example utilisation

- Ouvrir un invite de commande
- Aller dans le dossier src

- Recuperation de la commande via script
```shell
python3 recuperation_commande.py WEBSITE_LOGIN PASSWORD
```
  - WEBSITE_LOGIN identifiant du site
  - PASSWORD mot de pass associ
  - Si plusieurs commandes sont en cours, il va demande dans le terminal de choisir la commande à utiliser
  
- Fichier de sortie généré sorties/nousrire_commande.txt

- Executer la commande
```shell
python3 lanceur.py
```

- Le fichier de sortie s'ouvre automatiquement dans le navigateur

- Fichier actuel créé dans le dossier sorties du projet avec le fichier commande_prepare.html dans sorties

## Limites
Pas connues


## Historique versions

* 0 Pas de version

## Meta

Voir ``LICENSE`` pour plus d'information.

## Contribution

Fork du projet
```shell
git clone https://gitlab.com/jingl3s/EmballeTonVrac.git
```
### Dossiers
- A Faire

# Liens

