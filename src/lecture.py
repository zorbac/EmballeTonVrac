#!/usr/bin/python3
# -*-coding:utf8;-*-
"""
@author: jingl3s

"""

# license
#
# Producer 2018 jingl3s
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import csv
import json
import logging
import os

from packages import generate_cmd_html, generate_command_material


class PreparationCommandeNourire:
    def __init__(self):
        """
        Constructor
        """
        self._logger = logging.getLogger(self.__class__.__name__)

    def run_main(self, p_s_csv_config, p_s_csv_command):
        """

        :param p_s_csv_config:
        :param p_s_csv_command:
        """
        dict_produit = dict()

        # chargement configuration
        with open(p_s_csv_config) as fichier:
            reader = csv.DictReader(fichier)
            for line, row in enumerate(reader):
                if 0 == line:
                    quantite_contenant = row
                    for prod, quant in quantite_contenant.items():
                        try:
                            value = float(quant.replace(",", "."))
                            quantite_contenant[prod] = value
                        except:
                            pass

                if 1 == line:
                    dispo_contenant = row
                    for prod, quant in dispo_contenant.items():
                        try:
                            value = int(quant.replace(",", "."))
                            dispo_contenant[prod] = value
                        except:
                            pass

                elif 2 < line:
                    dict_produit[row["Produit"]] = row
                    dict_produit[row["Produit"]]["Volume par achat"] = float(
                        dict_produit[row["Produit"]]["Volume par achat"].replace(
                            ",", "."
                        )
                    )

        self._logger.info("Quantite contenant: {}".format(quantite_contenant))
        dict_cmde_produit, commande_info = self._extrait_commande(p_s_csv_command)

        # list_contenants_dispo = list()
        # b_list_contenant_definie = False
        dict_courses_contenant = dict()

        for produit, cmde in dict_cmde_produit.items():
            if produit == "":
                break
            self._logger.debug(
                "-----------------------\nTraitement de : {}, quantite : {}".format(
                    produit, cmde["quantite"]
                )
            )

            if produit == "huile d’olive":
                pass

            list_contenant = list()
            if not produit in dict_produit:
                raise TypeError(
                    "Produit demandé n'existe pas dans la configuration : {}".format(
                        produit
                    )
                )
            else:
                donnee = dict_produit[produit]

                # Extraction des contenants configurés pour se produit
                for ind, val in enumerate(donnee):
                    if val not in [
                        "Produit",
                        "Volume par achat",
                        "Groupe",
                        "Commentaire",
                        "Cocher les contenants restreints aux produits",
                    ]:
                        if donnee[val] != "":
                            list_contenant.append(val)

                self._logger.debug("Configuration pour produit : {}".format(donnee))
                self._logger.debug(
                    "Contenants associés au produit : {}".format(list_contenant)
                )
                self._logger.debug(
                    "Quantite contenants dispo : {}".format(dispo_contenant)
                )

                if len(list_contenant) == 0:
                    raise ValueError(
                        "aucune configuration trouve pour {}".format(produit)
                    )
                else:
                    list_contenant_utilise = list_contenant

                self._logger.debug(
                    "Volume par achat : {}".format(donnee["Volume par achat"])
                )
                quantite_achat = donnee["Volume par achat"] * int(cmde["quantite"])

                quantite_achat_en_cours = quantite_achat

                # FIXME Ajouter boucle tant qu'il y a du contenu à rentrer
                # Penser au cas ou fait 0 mais reste rentre dans le contenant

                debug_contenant = "Masson huile"

                while quantite_achat_en_cours > 0:
                    dict_sol = dict()
                    for contenant in list_contenant_utilise:
                        if not isinstance(quantite_contenant[contenant], float):
                            raise ValueError(
                                "Configuration contenant non defini pour le volume : {}".format(
                                    contenant
                                )
                            )

                        resultat = divmod(
                            quantite_achat_en_cours, quantite_contenant[contenant]
                        )

                        # Verification contenant disponible
                        # Gestion du cas 0 pour pondération car signifie rentre dans un
                        # seul
                        if debug_contenant == contenant:
                            self._logger.debug(dispo_contenant[contenant])
                            self._logger.debug(int(resultat[0]))

                        if (
                            0 < resultat[0]
                            and dispo_contenant[contenant] >= resultat[0]
                        ) or (0 == resultat[0] and 1 <= dispo_contenant[contenant]):

                            # TODO ajouter la gestion des préférences de contenants pour la pondération
                            # nb cont + % rest sur vol achat
                            pourc_cont_next = (
                                resultat[1] / quantite_contenant[contenant]
                            )
                            pond_next = 0
                            if 0 == resultat[0]:
                                if pourc_cont_next != 0:
                                    pond_next = 1 / pourc_cont_next
                            ponderation = (
                                resultat[0] * 3
                                + pond_next
                                + resultat[1] / quantite_achat_en_cours
                            )

                            #     ponderation = resultat[0] + \
                            #        resultat[1] / quantite_achat_en_cours
                            dict_info = dict()
                            dict_info["contenant"] = contenant
                            dict_info["nbre"] = int(resultat[0])
                            dict_info["reste"] = resultat[1]
                            dict_info["pond_next"] = pond_next

                            if ponderation not in dict_sol:
                                dict_sol[ponderation] = dict_info
                            else:
                                self._logger.debug(
                                    "ERR ponderaction déja trouvé, celle-ci est ignorée pour : {}".format(
                                        contenant
                                    )
                                )
                        else:
                            # fixme ajputer gestion aucun contenant dispo pour finir de
                            # remplir
                            pass
                        # Intelligence a definir ici
                        # https://www.wikiwand.com/en/Bin_packing_problem

                    #             Si le reste est suprieur à X% du contenant alors accepté pour le contenant
                    #             Verifier le nombre dispo pour choisir

                    #             https://github.com/strizhechenko/bar-chooser/blob/master/bar_chooser/__init__.py
                    # return sum(min(self.params[key], params[key]) for key in sel

                    self._logger.debug("---------------")

                    self._logger.debug(
                        "dict sol: {}".format(
                            json.dumps(dict_sol, indent=4, sort_keys=True)
                        )
                    )
                    if 0 == len(dict_sol):
                        raise ValueError(
                            "Produit choisi '{}' ne peut trouver un contenant".format(
                                produit
                            )
                        )

                    pond_choisie = min(dict_sol.keys())
                    self._logger.debug(
                        "Ponderation choisie : {}, contenant : {}".format(
                            pond_choisie, dict_sol[pond_choisie]
                        )
                    )

                    # Mise à jour du reste
                    if 0 == dict_sol[pond_choisie]["nbre"]:
                        quantite_achat_en_cours = 0
                    else:
                        quantite_achat_en_cours = dict_sol[pond_choisie]["reste"]

                    if produit not in dict_courses_contenant:
                        dict_courses_contenant[produit] = dict()

                    if len(dict_courses_contenant[produit]) == 0:
                        dict_courses_contenant[produit] = dict()
                    nbre_contenants = 0

                    if debug_contenant == dict_sol[pond_choisie]["contenant"]:
                        self._logger.debug(dispo_contenant[debug_contenant])
                        self._logger.debug(nbre_contenants)

                    if (
                        dict_sol[pond_choisie]["contenant"]
                        not in dict_courses_contenant[produit]
                    ):
                        dict_courses_contenant[produit][
                            dict_sol[pond_choisie]["contenant"]
                        ] = dict_sol[pond_choisie]
                        if 0 == dict_sol[pond_choisie]["nbre"]:
                            nbre_contenants = 1
                        else:
                            nbre_contenants = dict_sol[pond_choisie]["nbre"]
                        dict_courses_contenant[produit][
                            dict_sol[pond_choisie]["contenant"]
                        ]["nbre"] = nbre_contenants
                    else:
                        if 0 != dict_sol[pond_choisie]["nbre"]:
                            nbre_contenants = dict_sol[pond_choisie]["nbre"]
                            dict_courses_contenant[produit][
                                dict_sol[pond_choisie]["contenant"]
                            ]["reste"] = dict_sol[pond_choisie]["reste"]
                        else:
                            nbre_contenants = 1
                            dict_courses_contenant[produit][
                                dict_sol[pond_choisie]["contenant"]
                            ]["reste"] = 0.0
                        dict_courses_contenant[produit][
                            dict_sol[pond_choisie]["contenant"]
                        ]["nbre"] += nbre_contenants
                    self._logger.debug(
                        "Contenant:{}".format(dict_sol[pond_choisie]["contenant"])
                    )
                    dispo_contenant[
                        dict_sol[pond_choisie]["contenant"]
                    ] -= nbre_contenants

        # Preparation pour affichage des courses
        self._logger.debug("Courses : {}".format(dict_courses_contenant))

        dict_finale = dict()
        dict_contenants_utilise = dict()
        for produit, contenu in dict_courses_contenant.items():

            groupe = dict_produit[produit]["Groupe"]
            if groupe not in dict_finale:
                dict_finale[groupe] = list()

            prod_info = dict()
            prod_info[produit] = contenu
            dict_finale[groupe].append(prod_info)

            for contenant in contenu:
                if debug_contenant == contenant:
                    pass

                if contenant not in dict_contenants_utilise:
                    dict_contenants_utilise[contenant] = 0

                dict_contenants_utilise[contenant] += contenu[contenant]["nbre"]

        self._ecrit_resultat(
            dict_finale, dict_contenants_utilise, dict_cmde_produit, commande_info
        )

    def _extrait_commande(self, p_s_csv_command):
        """

        :param p_s_csv_command:
        """
        # recuperation commande
        dict_cmde_produit = dict()
        commande_info = list()
        with open(p_s_csv_command) as fichier:
            reader = csv.DictReader(fichier)
            for row in reader:

                # Processus de traitement du produit
                # Cas d'un fichier CSV comme sur le site
                if "Produit" in row:

                    produit = row["Produit"].strip().split()

                    if produit[0] != "|":

                        produit_local = produit[:-1]
                        nom_produit = " ".join(produit_local[:-2])
                        quantite = produit[-1]

                        row_content = dict()
                        row_content["quantite"] = quantite
                        row_content["Produit"] = nom_produit.lower()

                        dict_cmde_produit[nom_produit.lower()] = row_content
                    else:
                        new_row = [item for item in row.values() if item != "|"]
                        commande_info.append(new_row)

                else:
                    # Commande copiée d'internet dans un fichier texte
                    test = row["Commande"]
                    if len(row) == 1:
                        #             dict_cmde_produit[row["Produit"].strip()] = row
                        produit = test.strip().split()

                        if produit[-1].startswith("$"):
                            produit_local = produit[:-1]
                            nom_produit = " ".join(produit_local[:-3])
                            quantite = produit_local[-1:][0]
                    else:
                        produit = test.strip().split()
                        nom_produit = " ".join(produit[:-3])
                        quantite = produit[-1:][0]

                    row_content = dict()
                    row_content["quantite"] = quantite
                    row_content["Produit"] = nom_produit.lower()

                    dict_cmde_produit[nom_produit.lower()] = row_content

        if self._logger.isEnabledFor(logging.DEBUG):
            list_cmde = list()
            for nom, infos in dict_cmde_produit.items():
                cmde = nom + ", " + infos["quantite"]
                list_cmde.append(cmde)

            self._logger.debug("Commande : \n- {}".format("\n- ".join(list_cmde)))

        return dict_cmde_produit, commande_info

    def _ecrit_resultat(
        self, dict_finale, dict_contenants_utilise, dict_cmde_produit, commande_info
    ):
        """

        :param dict_finale:
        :param dict_contenants_utilise:
        """
        print("--------------------------------------------------")

        #        self._logger.debug(
        #                "params : \n- {}\n- {}\n- {}\n- {}\n".format(dict_finale, dict_contenants_utilise, dict_cmde_produit, commande_info))

        # nettoyage pour retirer chamos ignores ppur affichage
        dict_nettoyage = dict_finale.copy()
        for element in dict_nettoyage.values():
            for produit in element:
                for contenant in produit.values():
                    for critere in contenant.items():
                        del critere[1]["reste"]
                        del critere[1]["pond_next"]
                        del critere[1]["contenant"]
                        contenant[critere[0]] = critere[1]["nbre"]
                for nom_produit in produit.keys():
                    produit[nom_produit]["__QUANTITE__"] = dict_cmde_produit[
                        nom_produit
                    ]["quantite"]
        self._logger.debug(
            "Repartition: \n{}".format(
                json.dumps(dict_nettoyage, indent=2, sort_keys=True)
            )
        )

        # Format commande title to something readable
        commande_info_formate = list()
        self._logger.debug(commande_info)
        for item in commande_info:
            try:
                if isinstance(item[0], list):
                    ligne = item[1] + "," + ", ".join(item[0])
                else:
                    ligne = item[0] + "," + ", ".join(item[1])
            except Exception as e:
                self._logger.error(
                    "Error concatenating {} with exception {}".format(item, e)
                )

            commande_info_formate.append(ligne)
        commande_info_formate_s = "</BR>".join(commande_info_formate)

        export_cmd = generate_command_material.GenerateCommandMaterial()

        data = dict()
        data["INFO"] = commande_info_formate_s
        data["CMD"] = dict_nettoyage

        # output_contenants = json.dumps(
        #     dict_contenants_utilise, indent=2, sort_keys=True
        # )
        data["CONTENANTS"] = dict_contenants_utilise

        dict_cmde = dict_cmde_produit.copy()
        for element in dict_cmde.items():
            dict_cmde[element[0]] = element[1]["quantite"]

        data["COMMAND_DETAIL"] = dict_cmde

        export_cmd.set_open_browser(True)
        export_cmd.generate_file(data)

        # generate_cmd_html.generate_cmd_file(data)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    nousrire = PreparationCommandeNourire()

    # Old input file
    #     nousrire.run_main('nousrire_emballages_config.csv', 'nousrire_emballages_liste.txt')
    DOSSIER_CIBLE = "sorties"
    work_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", DOSSIER_CIBLE)
    )

    cfg_path = os.path.abspath(
        os.path.join(os.path.dirname(__file__), "..", "configuration")
    )
    # New input file generated by importer
    nousrire.run_main(
        os.path.join(cfg_path, "nousrire_emballages_config.csv"),
        os.path.join(work_path, "nousrire_commande.txt"),
    )


# EOF
