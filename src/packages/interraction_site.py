# -*-coding:utf8;-*-
"""
@author: jingl3s

"""

# license
#
# Producer 2018 jingl3s
# This code is free software; you can redistribute it and/or modify it
# under the terms of the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE (see the file
# LICENSE included with the distribution).

import csv
import logging
import os
from posix import mkdir

from bs4 import BeautifulSoup
from requests import session


class InterractionSite:

    """
    InterractionSite: Classe pour acceder au site de Nousrire
    """

    DOSSIER_CIBLE = "sorties"

    def __init__(self, p_url, p_test_mode=False):
        """
        Constructor
        """
        self._logger = logging.getLogger(self.__class__.__name__)
        self._url_site = p_url
        self._test_mode = p_test_mode
        self._work_path = os.path.abspath(
            os.path.join(os.path.dirname(__file__), "..", "..", self.DOSSIER_CIBLE)
        )

    def retrieve_cmd(self, p_login: str, p_pass: str):
        """ """
        with session() as c:
            if not os.path.exists(self._work_path):
                mkdir(self._work_path)
            retour_fonction = self._login(c, p_login, p_pass)
            if retour_fonction:
                commande = self._obtenir_commande(c)

                with open(
                    os.path.join(self._work_path, "nousrire_commande.txt"), "w"
                ) as file:
                    csv_w = csv.writer(file)
                    for l in commande:
                        csv_w.writerow(l)

        self._logger.info("Command retrieved")
        return retour_fonction

    def _login(self, requests_session, p_login, p_pass):
        login_success = False

        self._logger.info("Connection en cours")

        try:

            # Affiche la page principale par d�faut
            response_get = requests_session.get(self._url_site)

            if self._logger.isEnabledFor(logging.DEBUG):
                with open(os.path.join(self._work_path, "page_login.html"), "w") as f:
                    f.write(response_get.text)

            # Extraction de la valeur de salt mis � jour selon l'heure
            soup = BeautifulSoup(response_get.text, "html.parser")

            # Extract the value of login page
            v = soup.find_all("input", {"id": ["woocommerce-login-nonce"]})
            value_woocommerce = v[0].get("value")

            payload = {
                "username": p_login,
                "password": p_pass,
                "wp_http_referer": "/mon-compte/",
                "login": "Connexion",
                # TODO: Recuperer la valeur depuis la page de login
                "woocommerce-login-nonce": value_woocommerce,
            }
            response = requests_session.post(self._url_site, data=payload, verify=False)

            if self._logger.isEnabledFor(logging.DEBUG):
                # Contenu de la page au login r�cup�r�
                # Test montre un fonctionnement correct
                with open(os.path.join(self._work_path, "page_logged.html"), "w") as f:
                    f.write(response.text)

            soup = BeautifulSoup(response.text, "html.parser")

            # VErification si le login est un succes
            if "Salut" in response.text:
                self._logger.info("Login succes")
                login_success = True
            else:
                self._logger.error("Authentification erreur")

        except Exception:
            self._logger.exception("Error de connexion au serveur.")
            raise
        return login_success

    def _obtenir_commande(self, requests_session):

        self._logger.info("Recuperation commande en cours")

        # Acces a la page des commandes

        commandes_page = requests_session.get("https://nousrire.com/mon-compte/orders/")
        soup = BeautifulSoup(commandes_page.text, "html.parser")

        if self._logger.isEnabledFor(logging.DEBUG):
            with open(os.path.join(self._work_path, "page_commandes.html"), "w") as f:
                f.write(commandes_page.text)

        table = soup.find("table", {"class": "woocommerce-orders-table"})

        # Source:
        # https://stackoverflow.com/questions/23377533/python-beautifulsoup-parsing-table#23377804
        table_body = table.find("tbody")
        data = list()
        rows = table_body.find_all("tr")
        for row in rows:
            cols = row.find_all("td")
            cols = [ele.text.strip() for ele in cols]
            # Get rid of empty values
            data.append([ele for ele in cols if ele])

        list_commandes = list()
        list_commandes_total = list()
        for row in data:
            if row[2] == "En cours":
                list_commandes.append(row)
            list_commandes_total.append(row)

        if 0 == len(list_commandes):
            list_commandes = list_commandes_total
        # else:
        # self._logger.debug("Liste commandes total {}".format(list_commandes_total))
        self._logger.debug("Liste commandes {}".format(list_commandes))
        cmde_number, intitule = self._select_command(list_commandes)

        # cmde_number = 424717

        # URL acces detail commande
        commande_detail = requests_session.get(
            "https://nousrire.com/mon-compte/view-order/{}".format(cmde_number)
        )

        contenu_page = commande_detail.text

        if self._logger.isEnabledFor(logging.DEBUG):
            with open(
                os.path.join(self._work_path, "page_commande_detail.html"), "w"
            ) as f:
                f.write(contenu_page)

        contenu_commande = self._extract_command_content(contenu_page)

        # intitule = "Commande test"

        # Add title of command at end
        contenu_commande.append(f"[|{intitule}")

        return contenu_commande

    def _select_command(self, list_commandes):

        if len(list_commandes) == 1:
            intitule_commande = list_commandes[0]
        else:
            commandes_affichage = list()
            for indice, cmde in enumerate(list_commandes):
                commande = list()
                commande.append(str(indice))
                commande.extend(cmde)
                commandes_affichage.append("-".join(commande))

            print("\r".join(commandes_affichage))
            cmde_index = input("Fournir l'indice de la commande � utiliser : ")
            cmde_index_int = int(cmde_index)
            if cmde_index_int < len(list_commandes):
                intitule_commande = list_commandes[cmde_index_int]
            else:
                raise ValueError("Index en dehors des valeurs disponibles")

        cmde_extract = intitule_commande[0]

        cmde_extract_clean = cmde_extract.replace("#", "")

        cmde_number_retour = int(cmde_extract_clean)
        return cmde_number_retour, intitule_commande

    def _extract_command_content(self, page_web_detail_commande):
        """

        :param page_web_detail_commande: string of webpage containing the commande
        :return: list of command
        """
        soup = BeautifulSoup(page_web_detail_commande, "html.parser")

        table = soup.find(
            "table",
            {
                "class": "woocommerce-table woocommerce-table--order-details shop_table order_details"
            },
        )

        data = list()

        # Source:
        # https://stackoverflow.com/questions/23377533/python-beautifulsoup-parsing-table#23377804

        # Titre colonnes
        table_head = table.find("thead")
        rows = table_head.find_all("tr")
        for row in rows:
            cols = row.find_all("th")
            cols = [ele.text.strip() for ele in cols]
            # Get rid of empty values
            data.append([ele for ele in cols if ele])

        # Contenu
        table_body = table.find("tbody")
        rows = table_body.find_all("tr")
        for row in rows:
            cols = row.find_all("td")
            cols = [ele.text.strip().replace("*", "") for ele in cols]

            # Get rid of empty values
            data.append([ele for ele in cols if ele])

        if len(data) > 0:

            last_line = data[-1][0].split(",")[0]
            if "Point de cueillette" in last_line:
                last_line = last_line.replace("\n", ", ")
                last_line = last_line.replace("'", " ")
                last_line = last_line.replace(",", " ")
                data[-1] = ["|", last_line, "Test"]

        return data


# if __name__ == "__main__":

#    logging.basicConfig(level=logging.DEBUG)
#    manip = InterractionSite("", False)

#    list_commandes = [['n�133398', '15 septembre 2018', 'En cours',
#                       '$252.01 pour 19 articles', 'VoirTcharger la facture (PDF)']]
#    list_commandes.append(['n�133398', '15 septembre 2018', 'En cours',
#                           '$252.01 pour 19 articles', 'VoirTcharger la facture (PDF)'])

#    with open("page_commande_detail.html") as f:
#        text = f.read()

#    cmde = manip._extract_command_content(text)
#    cmde.append(list_commandes[0])
#    cmde[-1].insert(0, "|")
#    with open("nousrire_commande.txt", "w") as file:
#        csv_w = csv.writer(file)

#        for l in cmde:
#            csv_w.writerow(l)

#    print(cmde)
